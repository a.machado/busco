**BUSCO - Benchmarking sets of Universal Single-Copy Orthologs.**

To get help, ``python BUSCO.py -h`` and ``python BUSCO_plot.py -h``.

See also the user guide: BUSCO_v2.0_userguide.pdf 

You can download BUSCO datasets on http://busco.ezlab.org

*Copyright (C) 2016 E. Zdobnov lab*

BUSCO is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version. See <http://www.gnu.org/licenses/>

BUSCO is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
